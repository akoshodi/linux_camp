
Add the Employee Handbook to the nfs share.
Add a symlink to all user desktops.
Modify /etc/skel so that all new users will have the symlink.

From W1-------------------------------------------------------
 $ touch /shared/EmployeeHandbook.txt
 $ echo "Don't Be a Dick" > /shared/EmployeeHandbook
 $ cat /shared/EmployeeHandbook

Now lock the file so that no one can write to it.
 $ chmod a-w /shared/EmployeeHandbook  
Note: in the line above, a=all ugo; -=off; w=write
Note: this means, "for all, turn off write"
Now for the symlinks
Try
 $ cat /etc/passwd | cut -f 1,3 -d : | tr ':' ' ' | while read user uid; do echo user=$user uid=$uid; done
Try - this might work better:
 $ echo "cat /etc/passwd | cut -f 1,3 -d : | tr ':' ' ' | while read my_user my_uid; do if [[ $my_uid -gt 999 ]]; then echo $my_user $my_uid; fi; done" >> create_desktop_symlinks
NOTE: USE DAVID'S SCRIPT (PARTIALLY LISTED BELOW) FOR create_desktop_symlink
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
THIS SCRIPT WILL LOOK AT ALL USERS WHOSE UUID IS 1000 OR GREATER.
THEN IT WILL CHECK TO SEE IF THE USER HAS A /HOME/USER/DESKTOP DIRECTORY.
IF IT DOESN'T EXIST, THE ~/DESKTOP DIRECTORY WILL BE CREATED.
FINALLY A SIMLINK WILL BE SET UP IN THE USER'S ~/DESKTOP DIRECTORY
THAT POINTS TO THE EMPLOYEE HANDBOOK.

#!/bin/bash -eu
cat /etc/passwd | cut -f 1,3,6 -d : | tr ':' ' ' | while read my_user my_uid my_home
do
  if [[ $my_uid -gt 999 ]] && [[ $my_user != "nobody" ]]
  then
    echo "$my_user $my_uid $my_home"
    if ! [[ -d $my_home/Desktop ]]
    then
      sudo mkdir $my_home/Desktop
      sudo chown $my_user $my_home/Desktop
    fi
    my_command="sudo ln -sv /shared/EmployeeHandbook.txt $my_home/Desktop/"
    echo $my_command
    eval $my_command
  fi
done
# YOU NEED TO ALSO ADD IN SOME CODE THAT WILL ADD THIS TO /ETC/SKEL
# SO THAT ANY NEW USERS WILL OBTAIN THIS SYMLINK.
# SEE DAVID'S SCRIPT TO GET THIS PART OF THE CODE.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

NOTE: THIS DOES NOT RELATE TO THIS LAB BUT IS AN INTERESTING SIDE-NOTE.
IF YOU WISH TO MAKE CUSTOMIZATIONS TO ALL USER .BASHRC FILES, SUCH AS
CUSTOM ALIAS'S, YOU SHOULD EDIT /ETC/BASH.BASHRC SINCE THIS IS THE GLOBAL
.BASHRC FILE THAT /ETC/SKEL WILL USE WHEN CREATING A NEW USER.



