
===> BASIC - MOUNTED VOLUMES (ls /dev/[sh]d?) <===
/dev/sda
/dev/sdb


===> DISPLAYING MOUNTED PARTITIONS (df -hT) <===
Filesystem                           Type      Size  Used Avail Use% Mounted on
/dev/mapper/ClassMaster2015--vg-root ext4      6.5G  1.5G  4.7G  24% /
none                                 tmpfs     4.0K     0  4.0K   0% /sys/fs/cgroup
udev                                 devtmpfs  486M   12K  486M   1% /dev
tmpfs                                tmpfs     100M  500K   99M   1% /run
none                                 tmpfs     5.0M     0  5.0M   0% /run/lock
none                                 tmpfs     497M     0  497M   0% /run/shm
none                                 tmpfs     100M     0  100M   0% /run/user
/dev/sda1                            ext2      236M   38M  186M  17% /boot


===> DISPLAYING MOUNTED PARTITIONS (ls -lR /dev/disk) <===
/dev/disk:
total 0
drwxr-xr-x 2 root root 240 Aug 17 10:12 by-id
drwxr-xr-x 2 root root 100 Aug 17 10:12 by-uuid

/dev/disk/by-id:
total 0
lrwxrwxrwx 1 root root  9 Aug 17 10:12 ata-VBOX_CD-ROM_VB2-01700376 -> ../../sr0
lrwxrwxrwx 1 root root  9 Aug 17 15:46 ata-VBOX_HARDDISK_VB25465d28-0fd473af -> ../../sda
lrwxrwxrwx 1 root root 10 Aug 17 10:12 ata-VBOX_HARDDISK_VB25465d28-0fd473af-part1 -> ../../sda1
lrwxrwxrwx 1 root root 10 Aug 17 10:12 ata-VBOX_HARDDISK_VB25465d28-0fd473af-part2 -> ../../sda2
lrwxrwxrwx 1 root root 10 Aug 17 10:12 ata-VBOX_HARDDISK_VB25465d28-0fd473af-part5 -> ../../sda5
lrwxrwxrwx 1 root root  9 Aug 17 15:50 ata-VBOX_HARDDISK_VBb01379c3-0e4635bb -> ../../sdb
lrwxrwxrwx 1 root root 10 Aug 17 15:42 dm-name-ClassMaster2015--vg-root -> ../../dm-0
lrwxrwxrwx 1 root root 10 Aug 17 15:42 dm-name-ClassMaster2015--vg-swap_1 -> ../../dm-1
lrwxrwxrwx 1 root root 10 Aug 17 15:42 dm-uuid-LVM-JVQxQrcK7UspQ1NAS7mlWEj4XN50p2dbn0Xg2xgm1Xspkrk5j44xnXi9ss5YvPeG -> ../../dm-0
lrwxrwxrwx 1 root root 10 Aug 17 15:42 dm-uuid-LVM-JVQxQrcK7UspQ1NAS7mlWEj4XN50p2dbs0mGV7DgbsjT0lznlecaQ3lwtuLVwufm -> ../../dm-1

/dev/disk/by-uuid:
total 0
lrwxrwxrwx 1 root root 10 Aug 17 15:42 6085926e-5efc-4652-9a6e-4d2ee4478555 -> ../../dm-1
lrwxrwxrwx 1 root root 10 Aug 17 15:42 867ab9f4-c703-47ab-8e98-d52e3ae06583 -> ../../dm-0
lrwxrwxrwx 1 root root 10 Aug 17 10:12 aa6e8aa2-b79f-4831-8a4c-61f4e6e10579 -> ../../sda1


===> LIST BLOCK PARTITIONS (lsblk) <===
NAME                                  MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda                                     8:0    0     8G  0 disk 
├─sda1                                  8:1    0   243M  0 part /boot
├─sda2                                  8:2    0     1K  0 part 
└─sda5                                  8:5    0   7.8G  0 part 
  ├─ClassMaster2015--vg-root (dm-0)   252:0    0   6.7G  0 lvm  /
  └─ClassMaster2015--vg-swap_1 (dm-1) 252:1    0  1020M  0 lvm  [SWAP]
sdb                                     8:16   0    16G  0 disk 
sr0                                    11:0    1  1024M  0 rom  


