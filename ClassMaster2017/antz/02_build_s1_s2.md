# build s1 & s2 servers - answer key

**step 0**: type `ip a` command to obtain your IP address (should be next to something like enp0s3 or eth0). Write it down.

Also update packages:
* **for centos/redhat/fedora**: `sudo yum update`
* **for ubuntu**: `sudo apt update` and then `sudo apt upgrade`

***
## do same steps as build_w1 with different hostname and then:

* check to make sure ifcfg '-eth' or '-enp' files are set so network interfaces start on boot:

* **for centos/redhat/fedora**:
  * `cat /etc/sysconfig/network-scripts/ifcfg-enp0s3`

  * should say "ONBOOT="yes"". If not, then change it manually: `sudo vim /etc/sysyconfig/network-scripts/ifcfg-enp0s3`
  ... or use nmtui
  ... or use nmcli: `sudo nmcli con mod enp0s3 connection.autoconnect yes`
* **for ubuntu**:
  * `cat /etc/network/interfaces`
  * should say "auto enp0s3" right underneath text that reads "the primary network interface". If not then change it manually.


* go back to w1 and enter s1 ip address and info into /etc/hosts
  * first make sure you can ping s1: `ping 10.x.x.x` (use your actual IP address)
  * `sudo vim /etc/hosts`
  * 10.x.x.x    s1.yourhostname.test s1
  * now save and exit the file by clicking **shift+zz**
  * then make sure you can ping using the s1 hostname: `ping s1`

* `sudo ssh builder@s1` ssh from w1 into s1
  * enter s1 password... in!
