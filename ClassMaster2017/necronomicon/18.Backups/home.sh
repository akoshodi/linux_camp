#!/bin/bash -eux

DEST=/shared/backups/homefiles-$(date --iso)-$(hostname)

true mkdir $DEST

rsync -rltvH /home/ $DEST
# -r = recursive
# -l = copy symlinks as symlinks
# -t = preserve modification times
# -v = verbose
# -H = preserve hard links
