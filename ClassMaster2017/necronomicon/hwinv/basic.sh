#!/bin/bash -eu

echo "Memory"
echo "======"
free -m

echo ""
echo "Disk"
echo "======"
df -h | grep -v tmpfs
