Planning Notes

T-4m Open discussion and planning
T-3m Open registration
T-2m Close early bird
T-1m Close early PIF
T Run the event.

I'd like 2014 to be more ala-carte... My vision is that every cost-level from $0 to $2000+ will be available, with a contribution to SFS being entirely optional.
Something like this:
Study Guide: yes[]no[]
Lodging: self[] camp-site[] dorm[] cabin/yurt[] with appropriate pricing. Exam yes/no, want to teach a session, intend to participate in whole thing[] or days 1-3.

I want to teach outdoors, in a pavilion tent with open-sides, but probably low sides, to keep the light down to a bearable level. Also need a backup plan for extreme weather.

Plan 80 hours to read/rehearse and revise the material .

Keep it pair/triplet oriented. Need a minimum of 10 - 15 intermediate-level students to make this work. Advanced students OK, if well-behaved, but beginners will only slow things down.

Tuesday, August 20th evening 
pack and load vehicles 

Wednesday, August 21st evening 
drive up right after work, set up and test the equipment.

Need a proxy machine to connect and accelerate the Internet. (updates!) Set machines to the t-proxy as gateway.

In the weeks preceding Linux Camp, students will skim the textbook cover to cover and practice to brush up their Linux skills. The book SFS is recommending and providing to registered students is the 2013 release of the Linux+ Study Guide by Roderick W. Smith. The Linux+ exams LX0-101 and LX0-102 are identical to LPI-101 and LPI-102, so this is an effective study for us. 

In 2013, a study group was coordinated by our best enroller, Gary Romero.

Each day the students should have about 4? hours of hard work to do. (ie: The instructor must be able to do every exercise back-to-back in 16 hours or less.)
MUST do careful point-by-point of the LPIC-1 Objectives as it's own activity. The LPIC-1 Objectives coverage (and in the future the RHCSA objectives) will not sync to the labs. Don't even try.

Keep the labs story-oriented!