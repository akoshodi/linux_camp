#!/bin/bash -eu
#  Michael O'Lear

echo " "
echo "------------------------- HARDWARE  & STORAGE REPORT-------------------------"
echo " "
echo " "
echo " "

# Gather Kernel Version and Arch
echo "-------------------UNAME  Info-------------------"
uname -a
echo " "

# Show CPU Info
echo "-------------------CPU Info-------------------"
grep "vendor_id" /proc/cpuinfo
grep "model name" /proc/cpuinfo
grep "cpu cores" /proc/cpuinfo
grep "flags" /proc/cpuinfo
echo " "

# Show Memory Info
echo "-------------------MEMORY Info-------------------"
grep MemTotal /proc/meminfo
echo " "

# Show Video Card
echo "-------------------VIDEO Info-------------------"
lspci | grep VGA
echo " "

# Show Networking
echo "-------------------NETWORKING Info-------------------"
lspci -nn | grep -E 'Ethernet|Wireless'
echo " "

# Show USB
echo "-------------------USB Info-------------------"
lsusb -t
echo " "

#  Show Sound Card
echo "-------------------SOUND CARD Info-------------------"
lspci -nn | grep -E 'Audio'
echo " "

# Show Interrupts
echo "-------------------INTERRUPTS Info-------------------"
cat /proc/interrupts
echo " "

# Show DMA
echo "-------------------DMA Info-------------------"
cat /proc/dma
echo " "

# Show Disk Info
echo "-------------------DISK Info-------------------"
ls -lR /dev/disk/
echo " "

# Show Block Devices
echo "-------------------BLOCK DEVICE Info-------------------"
lsblk
echo " "

# Show Disk Space ... and More!
echo "-------------------DISK SPACE Info-------------------"
df -hT
echo " "

