#Delete users from a list

#!/bin/bash
PATH+=:/sbin:/usr/sbin:/usr/local/sbin

users="Jan Charlie Dani Eve Fred Gert"

#Delete Jan, Charlie, Dani, Eve, Fred, Gert
for user in $users
do
	userdel $user
done
