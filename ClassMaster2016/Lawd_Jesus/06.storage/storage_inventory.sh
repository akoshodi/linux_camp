#!/bin/bash -eu
# Matt James 8-21-2016

# Report all the physical storage devices
# For each disk:
#	 what sort of partition table 
# For each partition:
#	 file-system type
#	 size
#	 % used
#	 label
#	 serial-number
#	 and UUID.


# Lets get all of the physical disks:
for disk in /dev/[sh]d[[:alpha:]]
do
	echo "$disk has the following partitions: "
	short_dev=$(echo $disk | cut -f3 -d/)
	cat /proc/partitions | grep $short_dev[[:digit:]]*
done

# Get the partition info 
for part in /dev/[sh]d[[:alpha:]][[:digit:]]*
do
	short_part=$(echo $part | cut -f3 -d/)
	echo -n "$part filesystem is "
	echo \n
	echo -n "$short_part has UUID "
	ls -al /dev/disk/by-uuid/ | grep $short_part | awk '{print $9}'
	echo \n

done

# Get the partition size

# Get the partitoin %used
df -h | grep -Ev 'tmpfs|none|RackMaster'

# Get the partition label

# Get the serial-number

