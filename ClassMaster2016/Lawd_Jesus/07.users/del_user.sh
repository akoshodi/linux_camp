#!/bin/bash -eu
# Matt James Bitch!

# Delete the users

PATH+=:/sbin:/usr/sbin:/usr/local/sbin

users="Sam Jan Kim Alice Bob Charlie Dani Eve Fred Gert"
groups="contractors linux_admins"

for user in $users
do
	sudo userdel -r $user && 
		echo "removed $user" || 
		echo "unable to remove $user" 
done

for group in $groups
do
	sudo groupdel $group && 
		echo "removed $group" || 
		echo "unable to remove group $group" 
done
