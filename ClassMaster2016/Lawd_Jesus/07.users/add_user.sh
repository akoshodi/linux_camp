#!/bin/bash -eu
# Matt James Bitch!

# Create users
# make sure that they all have accounts on all the machines under 
# your control and that their UID's and GID's are consistent across machines.

# Please create user-accounts for yourselves, Jan, Kim, Sam, Charlie, Dani, Eve, Fred, and Gert.

# We also need two groups: 'contractors' should have Dani, Eve, and Fred, and 'linux_admins' 
# should have you two, me, and Eve.

# Don't worry! I'm just having her help out with some security work Sam wants done.

# Make sure all the contractors' accounts expire in six months.

PATH+=:/sbin:/usr/sbin:/usr/local/sbin

users="Jan Kim Sam Alice Bob Charlie Dani Eve Fred Gert"
groups="contractors linux_admins"
contractors="Dani Eve Fred"
linux_admins="Kim Alice Bob Eve"
exp_date=$(date -d "+1 day" +%F)

for user in $users
do
	sudo useradd -m $user && 
		echo "added $user" || 
		echo "unable to add $user" 
	echo -e "Freedom-16\nFreedom-16" | 
		sudo passwd $user
done

for group in $groups
do
	sudo groupadd $group && 
		echo "added $group" || 
		echo "unable to add group $group" 
done

for conman in $contractors
do
	sudo usermod -aG contractors $conman && 
		echo "added $conman to group contractors" || 
		echo "unable to modify user $conman"
	sudo chage -E $exp_date $conman &&
		echo "password now expires on $exp_date" ||
		echo "unable to set expiration date for $conman"
done

for admin in $linux_admins
do
	sudo usermod -aG linux_admins $admin && 
		echo "added $admin to group linux_admins" || 
		echo "unable to modify user $admin"
done
