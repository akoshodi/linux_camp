#!/bin/bash

# For each disk
# ? sort of partition table
# For each partition
# ? file-system type, size, % used, and identifiers, like label, serial-number, and UUID.

OLDPATH=$PATH
PATH=$PATH:/sbin:/usr/sbin

for DISK in $(lsblk | grep disk | awk '{print $1}'); do
  echo "Disk: /dev/$DISK"

  # Get the partition table type for this disk
  PART_TYPE=$(parted -l | grep /dev/$DISK -A2 | grep "Partition Table" | awk '{print $3}')
  echo "  Partition Table Type: $PART_TYPE"

  SERIAL=$(/sbin/udevadm info --query=property --name=$DISK | grep SERIAL_SHORT | awk -F= '{print $2}')
  echo "  Serial Number: $SERIAL"

  # Find all partitions on this disk
  for PARTITION in $(lsblk | grep part | tr -d '├─└' | awk '{print $1}'); do
    echo "  Partition: /dev/$PARTITION"

    # Get the filesystem type for this partition
    FS_TYPE=$(fdisk -l /dev/$DISK | grep $PARTITION | awk '{print $7}')
    echo "    Filesystem Type: $FS_TYPE"

    # Get the size of this partition
    PART_SIZE=$(lsblk /dev/$PARTITION | grep $PARTITION | awk '{print $4}')
    echo "    Size: $PART_SIZE"

    # Get the usage of this partition
    USAGE=$(df -h | grep $PARTITION | awk '{print $5}')
    echo "    Usage: $USAGE"

    # Get the label of this partition
    LABEL=$(e2label /dev/$PARTITION 2> /dev/null)
    echo "    Label: $LABEL"

    # Get the UUID of this partition
    UUID=$(blkid | grep $PARTITION | awk -F\" '{print $2}')
    echo "    UUID: $UUID"

  done

done

PATH=$OLDPATH

