#!/bin/bash -eu

cd /home
for user in *
do
	if ! [[ $user == 'lost+found' || $user == 'linux~' ]]
	then
		sudo mkdir $user/Desktop || echo "$user already has Desktop"
		sudo ln -sf /commons/employee_handbook.pdf $user/Desktop/
		sudo chown --recursive --reference $user/public_html $user/Desktop/
	fi
done


