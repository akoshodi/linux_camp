 
Physical Disk: sda
Partition Table: dos
 
---File System Type---
/dev/sda1 : Linux
/dev/sda2 : LVM
 
---Partition Size---
sda1 : 399M
sda2 : 15.6G
 
---Percent Used---
/dev/sda1 : 26%
 
---Partition UUID---
/dev/sda1:  UUID="290b7963-74f0-4ac7-a7f4-969d059658cc"
/dev/sda2:  UUID="aWielD-dmTC-IeEl-U2rp-VRa1-pJfg-l9IsJR"
  
