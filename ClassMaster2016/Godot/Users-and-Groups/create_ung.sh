#!/bin/bash -eu

PATH+=:/sbin:/usr/sbin:/usr/local/sbin

#Please create user-accounts for yourselves, me, Charlie, Dani, Eve, Fred, and Gert.
users="bob alice jan kim sam charlie dani eve fred gert"
for user in $users
do 
	sudo useradd -m $user
	echo -e "12345\n12345" | sudo passwd $user
done

#We also need two groups: 'contractors' should have Dani, Eve, and Fred, and 'linux_admins' should have you two, me, and Eve.
sudo groupadd contractors
for user in dani eve fred
do 
	sudo usermod -aG contractors $user
done

sudo groupadd linux_admins
for user in bob alice jan eve
do 
	sudo usermod -aG linux_admins $user
done

#Don't worry! I'm just having her help out with some security work Sam wants done.
for contractor in $( getent group contractors | cut -f 4 -d : | tr ',' ' ' )
do 	
	sudo usermod -e 2016-08-21 $contractor
done 
#After your account is on W1, please make sure you can easily send and receive email.
