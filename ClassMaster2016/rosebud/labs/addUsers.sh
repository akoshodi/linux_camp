#! /bin/bash -eu

# Please create user-accounts for yourselves, me, Charlie, Dani, Eve, Fred, and Gert.
#  We also need two groups: 'contractors' should have Dani, Eve, and Fred, and 'linux_admins' 
# should have you two, me, and Eve.



for user in alice bob jan kim sam charlie dani eve fred gert
do
    useradd -m $user
    echo -e "Freedom-16\nFreedom-16" | passwd $user
done

groupadd contractors
for user in dani eve fred
do 
   sudo usermod -aG contractors $user
done

groupadd linux_admins
for user in alice bob jan eve
do 
  sudo usermod -aG linux_admins $user
done


# Make sure all the contractors' accounts expire in six months.
for contractor in $(getent group contractors |
                     cut -f 4 -d : |
                     tr ',' ' ')
do
    usermod -e 2016-08-21 $contractor 
done

# After your account is on W1, please make sure you can easily send and receive email. It's 
# hugely important around here (as you can see).

