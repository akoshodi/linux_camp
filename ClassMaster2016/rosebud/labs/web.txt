#corporate websites
192.168.134.2 add dev.macguffins.test secure.macguffins.test
maguffins.test

corp.hosts.txt in commons has appropriate entries
add similar for my student site

for secure.macguffins you need a browser with sign in, which curl doesn't have.

curl = "see url"

need webserver on s2
sudo tasksel will let me pick bundles

make folder for dev in srv
echo "dev site" | sudo tee /src/dev/index.html

in
/etc/apache2/apache2.conf
uncomment srv block (last section)

in /etc/apache2/sites-available create dev.conf from ??
edit dev.conf
uncomment serverName, make it dev.rosebud.macguffins.test
documentRoot /src/dev
delete 7 lines above it
delete serverAdmin
get rid of all but serverName and DocumentRoot inside <virtualhost *:80>

*:80 says for requests from any ip on port 80 respond with content below

a2 utils create symlinks for me.
a2ensite enables site:  $sudo a2ensite dev

sudo apachectl configtest 
-do before taking web server down to ensure syntax passes
sudo service apache2 restart

Now have two sites, one responds to specific name, and one for all else

do site for prod.conf just as for dev
echo text into place holder page in /srv/prod/index.html
enable
	sudo a2ensite prod
check
	sudo apachectl configtest
restart
	sudo service apache2 restart
echo "this is production stuff" | sudo tee /srv/prod/index.html

/srv/secure/htaccess:
AuthType Basic
AuthName "Restricted Files"
AuthUserFile "/usr/local/apache/passwd/passwords"
AuthGroupFile "/usr/local/apache/passwd/groups"
Require group GroupName

put password & groups file someplace not servable (of course)
change to:
AuthType Basic
AuthName "rosebud"
AuthUserFile "/etc/apache2/rosebud.passwords"
AuthGroupFile "/etc/apache2/rosebud/groups"
Require group GroupName

"/etc/apache2/rosebud/groups":
CXO: jan kim sam bob

sudo htpasswd -c alicorn.passwords bob  (have to add at least one user to create)
htpasswd rosebud.passwords
sudo apt install apache2-utils
 
 allow site to use htpasswd:
 sudo vi /etc/apache2/apache2.conf
 add to /srv section:
 AllowOverride AuthConfig
 
 to allow access to group and not just user need 3 bits:
 (1)
	.htaccess instead of require user, change to Require group <groupname>
	AuthUserFile "/etc/apache2/rosebud.passwords"
	AuthGroupFile "/etc/apache2/rosebud.groups"
 (2)
 Need groups file /etc/apache2/rosebud.groups (space delimited)
 cxo:
 (3)
 sudo a2enmod authz_groupfile
 
secure.conf:
looks just like dev