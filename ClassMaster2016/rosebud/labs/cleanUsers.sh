# first ensure a clean slate
# None of these users have yet logged in
for user in alice bob jan kim sam charlie dani eve fred gert
do
    userdel -r $user
done
groupdel contractors
groupdel linux_admins
