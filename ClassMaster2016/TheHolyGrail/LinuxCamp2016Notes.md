﻿8/20/2016
Number: 4


1. Sudo chmod u+s $( which ping )
   1. Sets ping to be run by common user set uuid
1. Echo ipv4= $(Ip -4 addr show eth0 | grep -E ‘inet.*global’ | awk ‘{ print $2 }’ | cut -f 1 -d / )
   1. Cutting a search down to just cut out the ip address
1. Dpkg -S $( which hostnamectl )
   1. Finding out the owner of the service
1. Echo $ipv4 $fqdn $alias | sudo tee -a /etc/hosts
2. Adding a mount point with autofs
   1. Install autofs
   2. Vim /etc/auto.master
      1. Edit the file to enable /net     -hosts
   1. Start autofs
   2. cat /net/192.168.134.1/srv/commons/add-to-hosts | sudo tee -a /etc/hosts
      1. Adding host names to etc 
   1. sudo ln -sf /net/RackMaster/srv/commons /
      1. Adding mount point 
   1. Ll /commons/
      1. Check to see if its still mounted
1. Adding a public key
   1. Ssh-keygen
      1. Creates public keys and passphrase
   1. Ssh-copy-id <location>
      1. Copies the key to the server
1. Network Configuration CentOS
   1. Sudo nmtui
      1. Get into the graphical server editor in CentOS
   1. Edit connection enp0s3
   2. Ipv4 from auto to manual
   3. Add things
      1. Ip address 192.168.134.41/24 ←-nmtui specific
   1. Gateway
      1. 192.168.134.254
   1. DNS Servers
      1. 192.168.134.254
      2. 8.8.8.8
      3. 75.75.75.75
   1. Search Domains
      1. project name.macguffins.test
   1. /etc/sysconfig/network-scripts/ifcfg-enp0s3
      1. Actual file in centOS for network settings
   1. Update hosts file to show new ip.
1. Network Configuration Ubuntu
   1. /ect/network/interaces
      1. Edit this file to add in the network settings. Ex.
      2. auto eth0
      3. iface eth0 inet static
      4. address 192.168.1.100
      5. netmask 255.255.255.0
      6. network 192.168.1.0
      7. broadcast 192.168.1.255
      8. gateway 192.168.1.1
      9. dns-nameservers 192.168.1.1
1. Network Configuration openSUSE
   1. Use the gui
1. NAS Server
   1. Add Another Drive and Auto Mount 
      1. Pvdisplay - verbose pvs, vgs, lvs
         1. Shows the logical volume information
      1. Add a drive to virtualbox
      2. Use lsblk to check for disks
      3. Create partition with cfdisk <drive name>
         1. Change the type of the partician to LVM (8E)
      1. Run pvcreate on the partition
         1. EX: pvcreate /dev/sdb1 
      1. Add the pv to the vg using vgextend <vg to extend> <pv to be used>
      2. Create lv with lvcreate
         1. EX lvcreate <vg to take from>  -n <name> -l 50%FREE
      1. Add a filesystem
         1. mkfs.<filesystem> <lv you want to attach file system>  EX: sudo mkfs.xfs /dev/mapper/centos-lv_nas 
      1. Mount the device just to test
      2. Register in /etc/fstab to permanent mount
         1. Create a directory
         2. Make it imutable
            1. Sudo chattr +i /srv/nas
         1. Mount the drive to the new location
         2. Check proc mounts
            1. Cat /proc/mounts
            2. Tee into /etc/fstab
         1. Fstab
            1. What your mounting
            2. Where to mount it
            3. File system
            4. Defaults
            5. Does this filesystem need to be backup here
            6. Is the system responsible for the integrity
         1. Unmount the drive to force the system to process the new drive
         2. Reboot and test to make sure the drive remounts with fstab
        
   1. Create NFS export
      1. Install nfs kernel 
         1. Yum groups list -v to get the simple names of some of the groups you can install
         2. sudo yum groups install file-server
            1. Gets nfs samba and other 
      1. Make the config file
         1. /etc/exports
            1. /srv/commons *(rw,all_squash,no_subtree_check,insecure,anonuid=99,anongid=99)
      1. Start the service
         1. Sudo systemctl restart nfs
         2. Check the nfs status
         3. Systemctl list-unit-files nfs*
            1. Prints out the unit files that are running with nfs
         1. Systemctl restart nfs-server
            1. Starts the server within nfs
         1. Showmount -e 
            1. Shows mounts 
      1. Collapse the config file into the exports.d directory (for redhat only) in place of /etc/exports
         1. /etc/exports.d/nas.exports
      1. Test the mount from s2
         1. Sudo /sbin/mount.nfs4 s1:/srv/nas /mnt
            1. /sbin.mount.nfs4 <location of mount> <where you want to mount it on the current machine>
         1. Touch a file and check permissions
      1. Permissions
         1. Checks the permissions of the export
            1. Ls -ld .
         1. Change the permissions 
            1. Sudo chmod o+w .
      1. Enable the service to make it start all the time
         1. Systemctl enable nfs-server
   1. NFS mount or autofs and symlink
      1. Autofs mount
         1. Ln -s /net/S1.TheHolyGrail.MacGuffins.Test/srv/nas /TheHolyGrail
      1. Fstab mount
         1. Vim /etc/fstab
            1. <where the mount is coming from> <where the mount is happening> <mount type> <mount options> <backup?> <integrity>
               1. S1.TheHolyGrail.MacGuffins.Test:/srv/nas /alicorn nfs4 soft,intr 0 0
         1. Make the directory
         2. Make imuttable
            1. Chattr +r <directory>
         1. Mount the nfs to the mount point
            1. Mount -av 
               1. Mounts all that would be mounted on startup
   1. Layers of the disk
      1. PV - physical volumes
      2. VG - volume group
      3. LV - Logical Volume
1. Cron an rsync job
   1. Crontab
      1. Side notes
         1. Cron is used to schedule jobs to be run automatically
         2. Crontab -e to access the cron tab
         3. 5 fields
            1. <Minute> <hour> <day> <month> <day of the week>
         1. If you are looking to run a cron job for the systems cron, add a new file to /etc/cron.d/
   1. Writing the backup script
      1. Write the script in cron.hourly
         1. Do not use .sh DOES NOT WORK IN UBUNTU
      1. Rsync the nas server to the commons
         1. Switches he used -rltvH
         2. /commons/TheHolyGrail
         3. NOTE: always use trailing slashes for getting the things out of the directory
      1. Test using sudo bash <script>
      2. Add a logging section use >> to append
         1. Add a log every time the backup script runs
            1. rsync_job-$(dat --iso).log 2>&1
            2. Add a path to the log so it shows up where you want
      1. Make the file executable
         1. Chmod!
1. Apache (s2)
   1. Curl server
      1. Returns the webpage
   1. Add host entries
      1. www.macguffins.test dev.macguffins.test macguffins.test secure.macguffins.test
      2. Add the same entries for our s2
         1. Theholygrail.macguffins.test
         2. Dev.theholygrail.macguffins.test
         3. Secure.theholygrail.macguffins.test
         4. Prod.theholygrail.macguffins.test
         5. www.theholygrail.macguffins.test
   1. Install lamp stack
      1. Sudo tasksel select LAMP server
   1. Loading the first vhost
      1. Make directory dev on srv
      2. Echo info “dev site” into index.html
      3. Apache configuration /etc/apache2/apache2.conf
         1. Uncomment out saying that the srv structure is ok
      1. Go into sites available directory in apache2
         1. Cat the 000 file and tee it into the dev.conf
         2. Edit the dev.conf
            1. Uncomment server name
            2. Delete the 7 lines above it
            3. Delete server admin (only for example)
            4. Make server name dev.theholygrail.macguffins.test
            5. Document root /srv/dev
         1. Enabling the site dev
            1. A2ensite dev
      1. Config test
         1. Apachectl configtest
      1. Service apache2 restart 
      2. Check through the web browser to test dev
   1. Making the prod site
      1. Jump back into apache2 sites available 
      2. Copy the dev.conf to prod.conf
         1. Change the server name and document root
         2. Add a line ServerAlias www.theholygrail.macguffins.test theholygrail.macguffins.test
      1. Head back to srv to create the folder
         1. Make prod file
         2. Add the index.html file
      1. Enable the site
         1. A2ensite prod
      1. Check the syntax and reboot apache
         1. Apachectl configtest
   1. Secure site
      1. Make the vhost for secure
      2. Touch a file in /srv/secure called .htaccess
         1. AuthType Basic
         2. AuthName “Restricted Files”
         3. AuthUserFile “/etc/apache2/theholygrail.passwords”
         4. AuthGroupFile “/etc/apache2/theholygrail.groups”
         5. Require group CXO
      1. Create the groups file
         1. CXO: jan kim sam
      1. Create the password file
         1. Htpasswd -c theholygrail.passwords jan
         2. Create this for each CXO take away the -c
      1. Allow AuthConfig Override 
         1. Under the srv Directory in /etc/apache2/apache2.conf
         2. AuthConfig
      1. Enable apache module
         1. A2enmod
         2. Authz_groupfile
      1. Configtest and restart apache
1. Create shared directories
   1. Make directories 
   2. Change the group of the directories
      1. Using chgrp
   1. Change the permissions so that the the groups can write
      1. Use chmod for this
   1. Set the gid bit
      1. Chmod g+s linux_admins/
      2. Chmod g+s contractors/
   1. Take away access from linux admins 
   2. Test files
