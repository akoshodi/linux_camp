#!/bin/bash -eu
ipv4=$( ip -4 a | grep -E 'inet.*global' | grep -oE '[[:digit:]]{1,3}\.[[:digit:]]{1,3}\.[[:digit:]]{1,3}\.[[:digit:]]{1,3}/' | tr -d / )
fqdn=$( hostnamectl --pretty )
alias=$( echo $fqdn | cut -f 1 -d . )
echo $ipv4 $fqdn $alias
