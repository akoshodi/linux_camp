#!/bin/bash -eu

# make sure that they all have accounts on all the machines under your control and that their UID's and GID's are consistent across machines
# Create user-accounts for yourselves, me, Kim, Sam, Charlie, Dani, Eve, Fred, and Gert.
for user in alice bob charlie dani eve fred gert jan kim sam
do 
	sudo userdel -r $user
done

