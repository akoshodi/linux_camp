#!/bin/bash -eu
for file in /commons/corp-hosts.txt $( find /commons/ -maxdepth 2 -xtype f -iname "my*hosts*" )
do
  echo -e "\n# -------------------------------------------"
  echo "# --- $file ---"
  cat $file
  echo "# -------------------------------------------"
done
