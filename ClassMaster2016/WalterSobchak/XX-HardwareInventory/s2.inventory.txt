[H[2J
===> BASIC HARD DRIVE INFO <===
Model: ATA VBOX HARDDISK (scsi)
Disk /dev/sda: 8590MB
Sector size (logical/physical): 512B/512B
Partition Table: msdos

Number  Start   End     Size    Type      File system  Flags
 1      1049kB  256MB   255MB   primary   ext2         boot
 2      257MB   8589MB  8332MB  extended
 5      257MB   8589MB  8332MB  logical                lvm



===> DISPLAYING MOUNTED PARTITIONS (df -hT) <===
Filesystem              Type      Size  Used Avail Use% Mounted on
udev                    devtmpfs  234M  4.0K  234M   1% /dev
tmpfs                   tmpfs      49M  500K   49M   1% /run
/dev/dm-0               ext4      4.9G  1.6G  3.1G  34% /
none                    tmpfs     4.0K     0  4.0K   0% /sys/fs/cgroup
none                    tmpfs     5.0M     0  5.0M   0% /run/lock
none                    tmpfs     245M     0  245M   0% /run/shm
none                    tmpfs     100M     0  100M   0% /run/user
/dev/sda1               ext2      236M   73M  151M  33% /boot
rackmaster:/srv/commons nfs4       64G   39G   26G  60% /net/rackmaster/srv/commons


===> DISPLAYING MOUNTED PARTITIONS (df -hT) <===
/dev/disk:
total 0
drwxr-xr-x 2 root root 220 Aug 21 08:59 by-id
drwxr-xr-x 2 root root 100 Aug 21 08:59 by-uuid

/dev/disk/by-id:
total 0
lrwxrwxrwx 1 root root  9 Aug 21 09:00 ata-VBOX_CD-ROM_VB2-01700376 -> ../../sr0
lrwxrwxrwx 1 root root  9 Aug 21 16:07 ata-VBOX_HARDDISK_VBc3b624b6-4f4974da -> ../../sda
lrwxrwxrwx 1 root root 10 Aug 21 09:00 ata-VBOX_HARDDISK_VBc3b624b6-4f4974da-part1 -> ../../sda1
lrwxrwxrwx 1 root root 10 Aug 21 09:00 ata-VBOX_HARDDISK_VBc3b624b6-4f4974da-part2 -> ../../sda2
lrwxrwxrwx 1 root root 10 Aug 21 09:00 ata-VBOX_HARDDISK_VBc3b624b6-4f4974da-part5 -> ../../sda5
lrwxrwxrwx 1 root root 10 Aug 21 09:00 dm-name-S2--vg-root -> ../../dm-0
lrwxrwxrwx 1 root root 10 Aug 21 09:00 dm-name-S2--vg-swap_1 -> ../../dm-1
lrwxrwxrwx 1 root root 10 Aug 21 09:00 dm-uuid-LVM-7Fx76AFBwaaYVkMjVtQaAKkIhMg8aW0ZlpdwCVaL4f68KJ1Fx0Ac1rmAnMNnLd7P -> ../../dm-0
lrwxrwxrwx 1 root root 10 Aug 21 09:00 dm-uuid-LVM-7Fx76AFBwaaYVkMjVtQaAKkIhMg8aW0ZpV19rk3tSgW6f4bPW1q8CwBLCsg9PQsO -> ../../dm-1

/dev/disk/by-uuid:
total 0
lrwxrwxrwx 1 root root 10 Aug 21 09:00 49cdc2a3-d01f-47f4-a9b4-ee2addc97ad5 -> ../../dm-1
lrwxrwxrwx 1 root root 10 Aug 21 09:00 68c03104-d28e-4f78-b9c5-74f8c03fba21 -> ../../dm-0
lrwxrwxrwx 1 root root 10 Aug 21 09:00 7a4722fb-0f5c-4d69-b693-d2b429dd72ba -> ../../sda1


===> LIST BLOCK PARTITIONS (lsblk) <===
NAME                     MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda                        8:0    0     8G  0 disk 
├─sda1                     8:1    0   243M  0 part /boot
├─sda2                     8:2    0     1K  0 part 
└─sda5                     8:5    0   7.8G  0 part 
  ├─S2--vg-root (dm-0)   252:0    0   5.1G  0 lvm  /
  └─S2--vg-swap_1 (dm-1) 252:1    0   508M  0 lvm  [SWAP]
sr0                       11:0    1  1024M  0 rom  


===> PARTED (parted -l) <===


===> DISPLAY ATTRIBUTES OF VOLUME GROUPS (vgdisplay) <===


NAME="sda" FSTYPE="" LABEL="" MOUNTPOINT=""
NAME="sda1" FSTYPE="" LABEL="" MOUNTPOINT="/boot"
NAME="sda2" FSTYPE="" LABEL="" MOUNTPOINT=""
NAME="sda5" FSTYPE="" LABEL="" MOUNTPOINT=""
NAME="S2--vg-root (dm-0)" FSTYPE="" LABEL="" MOUNTPOINT="/"
NAME="S2--vg-swap_1 (dm-1)" FSTYPE="" LABEL="" MOUNTPOINT="[SWAP]"
NAME="sr0" FSTYPE="" LABEL="" MOUNTPOINT=""
NAME="ram0" FSTYPE="" LABEL="" MOUNTPOINT=""
NAME="ram1" FSTYPE="" LABEL="" MOUNTPOINT=""
NAME="ram2" FSTYPE="" LABEL="" MOUNTPOINT=""
NAME="ram3" FSTYPE="" LABEL="" MOUNTPOINT=""
NAME="ram4" FSTYPE="" LABEL="" MOUNTPOINT=""
NAME="ram5" FSTYPE="" LABEL="" MOUNTPOINT=""
NAME="ram6" FSTYPE="" LABEL="" MOUNTPOINT=""
NAME="ram7" FSTYPE="" LABEL="" MOUNTPOINT=""
NAME="ram8" FSTYPE="" LABEL="" MOUNTPOINT=""
NAME="ram9" FSTYPE="" LABEL="" MOUNTPOINT=""
NAME="loop0" FSTYPE="" LABEL="" MOUNTPOINT=""
NAME="loop1" FSTYPE="" LABEL="" MOUNTPOINT=""
NAME="loop2" FSTYPE="" LABEL="" MOUNTPOINT=""
NAME="loop3" FSTYPE="" LABEL="" MOUNTPOINT=""
NAME="loop4" FSTYPE="" LABEL="" MOUNTPOINT=""
NAME="loop5" FSTYPE="" LABEL="" MOUNTPOINT=""
NAME="loop6" FSTYPE="" LABEL="" MOUNTPOINT=""
NAME="loop7" FSTYPE="" LABEL="" MOUNTPOINT=""
NAME="ram10" FSTYPE="" LABEL="" MOUNTPOINT=""
NAME="ram11" FSTYPE="" LABEL="" MOUNTPOINT=""
NAME="ram12" FSTYPE="" LABEL="" MOUNTPOINT=""
NAME="ram13" FSTYPE="" LABEL="" MOUNTPOINT=""
NAME="ram14" FSTYPE="" LABEL="" MOUNTPOINT=""
NAME="ram15" FSTYPE="" LABEL="" MOUNTPOINT=""
