CRON AND RSYNC LAB
https://gitlab.com/sofreeus/linux_camp/blob/master/class/Lab-cron-n-rsync.md

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

We're going to use the S2 Ubuntu machine...

  $ crontab -l
  $ ll /etc/cron.{allow,deny}
  $ crontab -e  (this will allow you to edit your /etc/crontab file - don't use an editor like vim or nano...)

m h dom mon dow user command
minute
hour
day of month
month
day of week
user priviliges
command to perform

m h dom mon dow user command
* * * * * logger $( date )

==================================
From the crontab -e....
# Edit this file to introduce tasks to be run by cron.
#
# Each task to run has to be defined through a single line
# indicating with different fields when the task will be run
# and what command to run for the task
#
# To define the time you can provide concrete values for
# minute (m), hour (h), day of month (dom), month (mon),
# and day of week (dow) or use '*' in these fields (for 'any').#
# Notice that tasks will be started based on the cron's system
# daemon's notion of time and timezones.
#
# Output of the crontab jobs (including errors) is sent through
# email to the user the crontab file belongs to (unless redirected).
#
# For example, you can run a backup of all your user accounts
# at 5 a.m every week with:
# 0 5 * * 1 tar -zcf /var/backups/home.tgz /home/
#
# For more information see the manual pages of crontab(5) and cron(8)
#
# m h  dom mon dow   command
===================================

  $ sudo journalctl (works on other systems but not on debianish systems)
  $ sudo tail -f /var/log/syslog
  $ cd /etc
  $ ls -ld cron*
Typically if you want to add a cron fragment to your system, you would add it to the /etc/cron.d/ folder.

User crons go to /var/spool/crontab.
System crons go to /etc/crontab area (add as fragments to /etc/crontab.d/)

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Now let's create a backup script. David wants to put this on the S1 server to back up local server files to our /commons share.

Here are some rsync examples:
sudo rsync -avHP /alicorn/ /commons/alicorn/
sudo rsync -avHP --delete /alicorn/ /commons/alicorn/
Note: in the above line, the --delete will remove all of the orphan files from the destination - BE CAREFUL - THIS WILL DELETE WHATEVER FILES ARE AT YOUR DESTINATION LOCATION! You should move destination files to the source, clean them up, and then push all files to the destination using the --delete option.

  $ cd /etc/cron.hourly
  $ vim rsync_job

[begin]
#!/bin/bash -eu
# rsync -PHav /   (this doesn't work with our nas share)
# Note, always remember to use the trailing slashes with rsync.
# rsync -rltvH /srv/nas/ /commons/alicorn/
rsync -rltvH --delete /srv/nas/ /commons/alicorn/localnas/ >> rsync_job-$( date --iso ).log 2>&1
# 2>&1 means, send the error to the same location as the output.
#Note, some distros will not like it if your cron file names have a '.' in the name
[end]

 $ sudo bash rsync_job
Now view log file. Also view the /commons/alicorn/localnas directory to make sure that the backup took place.

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
OK, HERE'S WHAT I ACTUALLY DID.....
Create a backup script and put it somewhere, like in your home folder for instance.... and make the script executable (chmod +x backup-script.sh).

#!/bin/bash
rsync -rltvH --delete /srv/nas/ /commons/WalterSobchak/localnas/ >> /var/log/rsync_job-$( date --iso ).log 2>&1

Next, you'll want to go to /etc/cron.d/ and create a cron file, called rsync_job (or something useful).  Here's what you put in it:

# Example of job definition:
# .---------------- minute (0 - 59)
# |  .------------- hour (0 - 23)
# |  |  .---------- day of month (1 - 31)
# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# |  |  |  |  |
# *  *  *  i*  * user-name  command to be executed
# RUN EVERY 12 HOURS, EVERY DAY
0 */12 * * * root /home/user/backup-script.sh

# This means that the script will run every 12 hours at the top of the hour.

To test this, you might change the times so that it's ready to run in the next minute... so if it's currently 1:52 PM, then use:
53 * * * * ....

Then test by going to your /etc/cron.d/rsync_job script and executing it.
  $ bash rsync_job
Then go to your log file (/var/log/rsync_job...) and see if it ran.  Also look in the /commons/WalterSobchak/localnas/ directory and see if your files were copied over.

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

