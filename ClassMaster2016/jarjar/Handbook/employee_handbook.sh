#!/bin/bash
# Copy the employee handbook to users desktops and make sure it's there when they log in

user=$( whoami )
desktop="/home/$user/Desktop"
ls /home/$user/ | grep -i Desktop
if [ $? -ne 0 ]; then
	mkdir /home/$user/Desktop && cp /commons/employee_handbook.pdf /home/$user/Desktop/
else
	cp /commons/employee_handbook.pdf /home/$user/Desktop/
fi
chmod 440 /home/$user/Desktop/employee_handbook.pdf
