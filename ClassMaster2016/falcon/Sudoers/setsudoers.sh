#!/bin/bash -eux

echo '%linux_admins ALL=(ALL) NOPASSWD: ALL' | sudo tee /etc/sudoers.d/linux_admins
