#!/bin/bash -eu

login_defs=/etc/login.defs

if ! grep -q CREATE_HOME $login_defs; then
  echo CREATE_HOME yes >> $login_defs
fi

perl -i -pe '
  s/^UMASK\s+022/UMASK 002/;
  s/^CREATE_HOME\s+no/CREATE_HOME yes/;
  s/^USERGROUPS_ENAB\s+no/USERGROUPS_ENAB yes/;
' $login_defs
