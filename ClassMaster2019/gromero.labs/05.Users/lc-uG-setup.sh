#! /bin/bash -eu
#
# By Gary Romero
# 11/9/2019
# linux camp grinder edition

NEWUSERS='gary jan kim sam charlie dani eve fred gert'
NEWGROUPS='contractors linux_admins'
CONTRACTORS='dani eve fred'
ADMINS='gary jan eve'
UIDNO=2000
GIDNO=20000

for i in $NEWUSERS; do
	sudo useradd -u $UIDNO $i
	UIDNO=$(( $UIDNO + 1 ))
	echo "user $i added"
done

for i in $NEWGROUPS; do
	sudo groupadd -g $GIDNO $i
	GIDNO=$(( $GIDNO + 1 ))
	echo "group $i added"
done

for i in $CONTRACTORS; do
	sudo usermod -aG contractors $i
	sudo chage -E `date -d "180 days" +"%Y-%m-%d"` $i
	echo "users $i added to Contractors"
done

for i in $ADMINS; do
	sudo usermod -aG linux_admins $i
	echo "users $i added to ADMINS"
done


