The Linux Rosetta Stone
===

The package manager is:

- `rpm` in EL and SUSE
- `dpkg` in Debian'ish

The package repository manager is:

- `dnf` in Fedora and EL 8+
- `yum` in EL 7
- `zypper` in SUSE
- `apt` in Debian'ish, or `apt-get`, `synaptic`, or `aptitude`

To install a whole software stack, like LAMP:

- `yum groups install lamp-server` in EL
- `sudo zypper install -t pattern lamp_server` in SUSE
- `sudo tasksel install lamp-server` in Debian'ish

How do I Totally Update Everything?:

- CentOS 7:

  ```shell
  sudo yum clean all &&
  sudo yum -y upgrade
  ```

- CentOS 8 and Fedora:

  ```shell
  sudo dnf -y distrosync
  ```

- openSUSE:

  ```shell
  sudo zypper refresh
  sudo zypper -n dist-upgrade
  # or: sudo zypper -n dup
  ```

- ubuntu 20.04+:

  ```shell
  sudo apt update &&
  sudo apt -y dist-upgrade &&
  sudo apt -y autoremove
  ```

How do I search the package repositories
... for a binary, like ncdu, byobu, or git?

- CentOS 7:

  ```shell
  dnf provides "*bin/byobu"
  ```

- openSUSE:

  ```shell
  cnf byobu
  ```

- Ubuntu:

  ```shell
  byobu
  # then just do what it tells you
  ```

How do I search the package repositories
... for a non-program file, like the 'words' file or `/etc/auto.master`?

- CentOS 7:

  ```shell
  dnf provides "*dict/words"
  ```

- openSUSE:

  ```shell
  # zypper search only works on /bin/ /sbin/ and /etc/
  zypper search --provides "*dict/words" # does not work
  # So, how *do* you do it? I don't know.
  zypper search --provides "/etc/auto.master" # works
  ```

- Ubuntu:

  ```shell
  sudo apt install apt-file
  sudo apt-file update
  apt-file search dict/words
  ```

How do I figure out which package owns some file?

- rpm:

```shell
rpm -qf /usr/share/dict/words
```

- dpkg:

```shell
dpkg -S /usr/share/dict/words
```

How do I list services?

- SysV:

```shell
ls /etc/init.d/
```

- SystemD:

```shell
systemctl
# or
systemctl list-unit-files --type=service
```

How do I start, stop, restart, and status services?

- SysV:

```shell
sudo /etc/init.d/some-service [status|start|stop|restart]
```

- SystemD:

```shell
systemctl [status|start|stop|restart] some-service
```

How do I enable and disable daemons?

- SysV:

```shell
# Enterprise Linux and SUSE
sudo chkconfig some-service [on|off]
sudo chkconfig --list some-service

# Ubuntu / Debian
sudo update-rc.d some-service defaults # enable
sudo update-rc.d -f some-service remove # disable
```

- SystemD:

```shell
systemctl [enable|disable] some-service
```

Further reading on SysV vs SystemD:
- https://fedoraproject.org/wiki/SysVinit_to_Systemd_Cheatsheet
- https://lms.quickstart.com/custom/799658/SysVinit%20to%20Systemd%20Cheatsheet%20(Abbreviated%20Version).pdf
