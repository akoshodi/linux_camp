Lab: Full root partition incident

> FROM: Kim <kim@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> The root partition of your S1 server is getting pretty full. Best fix that.

> --Kim

---

### Process Notes

* record the output of `df -h /`
* shut it down
* add a disk
* add a partition of type 8e
* convert the partition to a PV
* add the PV to the VG
* extend the LV
* extend the file-system
* re-run `df -h /` and compare with the first run

### Hints (Binaries to use, Configs to modify)

- fdisk, cfdisk (recommended), parted, gparted
- pvcreate
- vgextend
- lvextend
- resize2fs or xfsgrow
- lvs
- pvs
- vgs
- lvdisplay
- vgdisplay
- pvdisplay
