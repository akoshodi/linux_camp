Lab: Virtualize like it's the current year
==========================================

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice and Bob,

> Please get started with Linux-native virt. I guess it's called KVM.
> How do you not get confused between the KVMs?
> Anyway, make a web-server that comes up when the host comes up.

> Thanks,

> Jan

---

virsh autostart

---

```bash
virsh list --all
sudo apt install libvirt-clients

sudo systemctl status libvirtd.service 

apt-file search libvirtd.service
sudo apt install apt-file
sudo apt-file update
apt-file search libvirtd.service

sudo apt install libvirt-daemon-system
sudo systemctl start libvirtd.service 
sudo systemctl status libvirtd.service 
tail /etc/group
virsh list --all

virt-manager
```