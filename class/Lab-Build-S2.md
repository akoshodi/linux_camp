Lab: Build S2
-------------

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> Please build another server. Make sure to NOT name the first user alice, bob, or jan. 'builder' is fine, or 'tmpadmin', or whatever.

> And don't forget to confirm that you can ssh to it.

> --Jan

---

Notes:

Verify that you can ssh to the new server. You might need to modify /etc/network/interfaces to make network interfaces start on boot.

--Kim

---

Suggested values:
- Distro: [Debian](https://www.debian.org/)
- vm name: S2
- hostname: S2.(student-project-name).test
- Memory: 512MB
- Storage: 8GB
- Network: (match W1)

Notes:

Set the hostname and reboot.

Verify that you can ssh to the new server from your W1, at least.

```shell
echo '6.7.8.9 s2.alicorn.test s2' | sudo tee -a /etc/hosts
ssh some-user@s2
```

Highly-motivated partners will ensure that they can ssh to one another's servers.

Hints
---

- /etc/network/interfaces
- /etc/netplan
- sudo ifdown eth0 ; sudo ifup eth0
- sudo service networking [stop|start|restart]
- hostnamectl
- ip addr show (ip a)
- sudo tasksel
- /etc/hosts
- sudo tee [-a]
