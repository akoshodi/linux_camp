Lab-The Sam Hammer
==================

> FROM: Sam <sam@macguffins.test>

> TO: Kim <kim@macguffins.test>, Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> CC: Jan <jan@macguffins.test>

> Please change the passwords to all shared accounts to unknown, complex values, remove all currently authorized keys from those accounts, and stop using them.

> Please do that immediately.

> After a reasonable period: a day, a week, a month at most, delete all the idle, non-root accounts, like 'builder'.

> Sam, CSO, MacGuffins

---

### Process hints

You heard the CSO.
