Lab: Mail matters
===

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> We keep missing important logs and alerts. We should be seeing something in our inboxes every time a cron or at job fires. The way it's set up now, backups could start failing and we wouldn't know until we wanted to do a restore!

> Please configure the servers to send root's mail to root's local mail spool and to some real person, somewhere.

> Thanks,

> --Jan

---

Notes:

`cron`, `at`, and `mail` should work "normally". That is, the owner of any cron or at job should receive the un-captured output and errors by mail, and the mail program should be able to send mail. If you want to get fancy, you could try using `uuencode` to create a mail-attachment.

---

Use cron to create a noise generator.

```
$ crontab -l
* * * * * date --iso=min
```

Use `mail` to check your local mail spool, rather than `tail` or `less` `/var/spool/mail/$USER`.

Use `echo "some test text" | mail -s "some subject" someone@somedomain.tld` to send instant tests.

---

The lines in `/etc/aliases` will go something like this:

```
bob:   \bob,someone@real-domain.tld
root:  \root,real-admin@real-domain.tld
alice: \alice,real-admin@real-domain.tld
```

Don't forget to run `newaliases` after changing `/etc/aliases`.

---

You may need to run one or both of the following Postfix config tweaks:

```
sudo postconf -e myhostname=$(hostname)     # Only needed on openSUSE?
sudo postconf -e relayhost=relay.domain.tld # Only needed when blocked
sudo postfix reload                         # Needed when config changes
# postconf modifies main.cf
```
