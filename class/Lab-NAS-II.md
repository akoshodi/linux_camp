Lab: NAS Part 2: Less insecure file-server
===

---------------------------------------------------------------------

> FROM: Sam <sam@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Did you guys really set up a wide open NFS export?  This should be locked down with team-specific directories.

> Please do that immediately.

> Sam, CSO, MacGuffins

---------------------------------------------------------------------

* Disable all_squash, leave root_squash
* Disable write access to /srv/student-project-name
* Create a subdirectory /srv/student-project-name/secrets that is only accessible by linux_admins
  * Hint: setgid
* Create a public subdirectory at /srv/student-project-name/pub that is world-writeable, but protect users' files from accidental deletions by others
  * Hint: sticky bit

### Hints on commands and configs

- yum
- showmount
- exportfs
- systemctl
- /etc/exports
- firewall-cmd
