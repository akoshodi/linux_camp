Lab: Dirty, Bad, Unreliable Disk
================================

---

> FROM: Kim <kim@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Folks,

> There's definitely something wrong with the root file-system on S2.
> It keeps going read-only. Tonight, take s2 down and run badblocks
> on the whole storage device backing root, then run whichever fsck
> matches the filesystem and let's hope it can last a few more weeks.

> Thanks,

> --K

---

### Process Notes

Boot the system from a rescue CD or stick, especially when repairing the root fs.

If you think the storage device is dying, *don't* run badblocks, run ddrescue, testdisk, or photorec. Every block you read could be your last.

When running badblocks, target the whole device (probably the whole disk), not a partition.

When running fsck, target the filesystem and use the fsck that matches it.

### Hints of commands and configs

https://www.system-rescue.org/Download/

Examples:

```bash
# list block devices
lsblk

# read-only, one good pass
sudo badblocks -svp1 /dev/mmcblk0
# non-destructive read-write
sudo badblocks -svn /dev/mmcblk0
# destructive read-write, big blocks
sudo badblocks -svwb 4096 /dev/usb0

# turn off LVM
sudo vgchange -an
# turn on LVM
sudo vgchange -ay
```

fsck.TAB-TAB