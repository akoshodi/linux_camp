Lab: Evil Eve
===

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> Ever since the contract ended, I've been getting the ~strangest~ calls from Eve. Why would I care what she's wearing and whether she's “packing heat”?

> Anyway, would you make sure that she can't login to any of the boxes? Her account should have expired by now, but just have a quick check, will you?

> Thanks,

> --Jan

---

Can Eve login? What if you reset her password? Does she have any authorized_keys? Would they work if she did? Are there any unusual services running on any of the boxen? Does Eve have any cron-jobs? Would they work if she did? When was the last time Eve logged in? What about Bob? Are there any weak passwords in use?
