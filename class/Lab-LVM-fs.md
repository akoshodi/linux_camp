Lab: LVM & Filesystems
===

-------------------------------------------------------------------------------

> FROM: StrongJan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> Can you set up a directory for storing some documents?  Let's just put it on S1 and we can all use it.  I don't know how much space we'll need, so use LVM and start small.  Don't go too crazy with the filesystem; use the same filesystem as your distro default.

> Thank you!

> --Jan

-------------------------------------------------------------------------------

### Process Notes

Consider checking /etc into git. It's a *great* way to roll-back undesired changes, and examine changes made by user-friendly GUI utilities, like YAST and system-config-*

**LVM**

1. Add a disc
*  Add a partition of type 8E (Linux LVM)
*  Create a PV of the partition
*  Extend the VG with the new PV (or create a new VG)
*  Create a new LV
*  Make a file-system on the LV
*  Make a mount point in /srv and mark it immutable
*  Mount the file-system to the mount-point
   Example:
   ```bash
   sudo mount /dev/mapper/root-alicorn /srv/alicorn
   ```
*  Register the file-system in /etc/fstab (recommend UUID)
*  Umount the manual mount and mount automatically
   Example:

   ```bash
   sudo umount /srv/alicorn &&
   sudo mount -av
   ```

   ### Hints on commands and configs

   - cfdisk
   - pvcreate
   - vgextend
   - lvcreate
   - mkfs
   - mount
   - /etc/fstab

   - getenforce
   - setenforce
   - chcon
   - restorecon
