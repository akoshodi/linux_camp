Lab: Limits
===========

> FROM: Sam <sam@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Eve showed Jan how any user can lock up any machine with a fork bomb.

> Pleasure ensure that that *can't* happen, before someone tries it on a server.

> --Sam

---

### Process Notes

As a normal user, lock up one of your machines with a fork bomb.

```bash
:(){ :|: & };:
```

Make sure you have *more than one* administrative login to each machine, in case you hang one of your logins. Setting good passwords on alice and bob and ensuring that they're both admins is one way to do it.

```
[bob@s1 ~]$ while pgrep -u alice; do sudo killall -s KILL -u alice; done
```

Create a process limit to ensure that that can't happen. Test that it worked.

Hints:
- man limits
- ulimit
- /etc/security/limits.conf or /etc/limits.conf

Unsolved:

There's supposed to be a way to do this with SystemD, but I can't find it. I have found a few articles about setting limits on services, but not on users.

Where do the limits already-in-place come from?