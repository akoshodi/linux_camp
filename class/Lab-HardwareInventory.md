Lab: Hardware Inventory
=======================

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> I'd like an inventory of all the hardware. For each computer, please give me at least the following information: processor, memory, network, and video. The more detail you can give me, the better. I definitely want the device id's for everything...

> Script as much of the work as you can. For instance, to report memory, you might use a command like this: `grep MemTotal /proc/meminfo`

> If you can't get at the information directly from bash, and you need help from the console operator, do something like this:

```
# Ask the user a question on stderr
echo 'Please run `free` in another window, read the number in the “Mem:” row, under “total” and input it here: ' >&2
# read the answer on stdin
read memory_total
# report the information on stdout
echo “Total Memory = $memory_total”
```

> Send me the script when you're done. If possible, I'd like the script to run correctly when invoked by a normal user.

> If it has to be run as root, say so in the comments, warn the operator, or just check whether the script is running as root and automatically call sudo if it isn't. It up to you.

> Thanks!

> --Jan

> P.S. Next week, we'll inventory storage!

---

### Process Notes

Look over the scripts from previous years. Do they still run correctly? (Did they ever?) Do you like the strategy or formatting? Can you use/modify it, or use what you learn by studying it, to build the best Hardware Inventory script, evar?

Create your script in /commons/(student-project-folder)/##.HardwareInventory/

Run the script on at least two *physical* machines. Virtual machines are much less interesting.

Use a live Linux distro to boot the machines that don't have Linux.

- How much memory does this system have?
- How many cores does this system have, and at what speed do they run?
- What kind of ethernet interface do you have? Make, model, device code
- ... graphics adapter?
- ... wireless adapter?
- ... sound (card)?
- How are your IRQ and DMA channels allocated?

Create an empty file named `done` in HardwareInventory/ when done.

### Hints

Text-processing commands:
- awk
- cut
- grep
- sed
- head
- tail
- wc

Kernel files:
- /proc/meminfo
- /proc/cpuinfo

Discovery programs:
- lspci
- lsusb
- dmesg
- journalctl
- lshw
- hwinfo

Dev-nodes: /dev/*

Logs:
- /var/log/dmesg
- /var/log/(messages|syslog)
