Lab: Storage Inventory
===

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob:

> I owe you a beer or a pizza. What's good is that since you gave me a script, rather than a spreadsheet, we can just run it again next year, or improve it.

> This task should be somewhat simpler.

> Can you send me a report of all the physical storage devices? For each disk, what sort of partition table does the device have? For each partition, give me file-system type, size, % used, and identifiers, like label, serial-number, and UUID.

> --Jan

---

### Hints

Create the script and the done file in ...(student-project-folder)/StorageInventory/

Binaries:
  lsblk, blkid, fdisk, parted, df, pvs, vgs, lvs, pvdisplay, vgdisplay, lvdisplay

Dev-nodes:
  /dev/disk/, /dev/[sh]d*

Configs:
  /etc/fstab

Kernel virtual-filesystem:
  /proc/mounts
