Lab: RAID
=========

> FROM: Kim <kim@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> It would be great to be able to keep filesystems online in the event of disk failure.

> Grab some hard drives from stock and create a RAID array on one of the servers. Later, we'll move the important files to it.

> --Kim

---

### Process Notes


Add disks to the VM

Optionally, create partitions of type "Linux RAID" on the disks

Aggregate disks, or partitions if you created them, into one or more RAID arrays.

Consider:

- for creating partitions, use fdisk, cfdisk, or parted
- gpt vs dos partition table is unimportant
- may want dos partition table when:
    * disk is < 2TB
    * < 5 partitions are wanted
- Make Linux RAID partitions or don't bother

man mdadm

Search for "EXAMPLES" (or just EXAM)

Consider RAID0, RAID1, RAID5, RAID6
https://en.wikipedia.org/wiki/Standard_RAID_levels
... RAID10
https://en.wikipedia.org/wiki/Standard_RAID_levels#Nested_RAID

```bash
sudo mdadm --create /dev/md/anarray --level=1 --raid-devices=2 /dev/sd[bc]1
cat /proc/mdstat
sudo mdadm --stop /dev/md/anarray
sudo mdadm --create /dev/md/battarray --level=5 --raid-devices=4 /dev/sd[bcde]1
cat /proc/mdstat
sudo mdadm --stop /dev/md/battarray
sudo mdadm --create /dev/md/creamerray --level=6 --raid-devices=4 /dev/sd[bcde]1
cat /proc/mdstat
sudo mdadm --stop /dev/md/creamerray
sudo mdadm --create /dev/md/deathray --level=10 --raid-devices=4 /dev/sd[bcde]1
cat /proc/mdstat
```

Save the config to /etc/mdadm.conf

```bash
sudo mdadm --detail --scan
sudo mdadm --detail --scan | sudo tee /etc/mdadm.conf
```

As always, test, reboot, and test again to ensure that your changes survive.
