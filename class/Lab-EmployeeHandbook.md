Lab: Employee Handbook
======================

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Please put a copy of the employee handbook on everyone's desktop. Apparently, some of our contractors don't know we have a dress-code that doesn't include pink sweats and bunnie slippers.

> I had this done last year, too, but none of the new employees have it. This year, please make sure it sticks.

> TY,

> --Jan

---

### Process Notes

Create symlinks on all the current users' desktops with a simple loop.

For new users, you'll need to modify the home-folder template. You'll probably need to create the Desktop directory in /etc/skel/. Then, create the sym-link in /etc/skel/Desktop, and create a new user to test.

You only need to do this on W1. On other machines, "Desktop" doesn't have as much meaning.